# InvocationResult
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transactionId** | [**Long**](long.md) |  | [optional] [default to null]
**invocationState** | [**InvocationState**](InvocationState.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

