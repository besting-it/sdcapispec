# DefaultApi

All URIs are relative to *http://sdcapi/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDescription**](DefaultApi.md#getDescription) | **GET** /provider/{epr}/get/description | Get the static description of an SDC provider.
[**getInvocationStates**](DefaultApi.md#getInvocationStates) | **GET** /provider/get/invocationstates/{epr}/{id} | Get a list of invocation states.
[**getSdcProviders**](DefaultApi.md#getSdcProviders) | **GET** /discovery | Get a list of currently active SDC providers.
[**getStates**](DefaultApi.md#getStates) | **GET** /provider/{epr}/get/states | Get a list of SDC provider states.
[**getValue**](DefaultApi.md#getValue) | **GET** /provider/{epr}/get/value/{handle} | Get a single state value
[**setState**](DefaultApi.md#setState) | **POST** /provider/{epr}/set/state | Set a state on an SDC provider
[**setValue**](DefaultApi.md#setValue) | **POST** /provider/{epr}/set/value/{handle} | Set a single state value


<a name="getDescription"></a>
# **getDescription**
> String getDescription(epr)

Get the static description of an SDC provider.

    Returns an MdDescription object containing the provider description.

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **epr** | **String**| The endpoint reference of the SDC provider | [default to null]

### Return type

[**String**](../Models/string.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/xml

<a name="getInvocationStates"></a>
# **getInvocationStates**
> List getInvocationStates(epr, id)

Get a list of invocation states.

    Returns a list of invocation states for a current transaction.

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **epr** | **String**| The endpoint reference of the SDC provider | [default to null]
 **id** | **Long**| The transaction id | [default to null]

### Return type

[**List**](../Models/InvocationState.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

<a name="getSdcProviders"></a>
# **getSdcProviders**
> List getSdcProviders()

Get a list of currently active SDC providers.

    Returns a list containing endpoint references of SDC providers available in the SDC network.

### Parameters
This endpoint does not need any parameter.

### Return type

[**List**](../Models/string.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

<a name="getStates"></a>
# **getStates**
> String getStates(epr, handle)

Get a list of SDC provider states.

    Returns an MdState object containing states

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **epr** | **String**| The endpoint reference of the SDC provider | [default to null]
 **handle** | **String**| An optional filter handle to retrieve a single state | [optional] [default to null]

### Return type

[**String**](../Models/string.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/xml

<a name="getValue"></a>
# **getValue**
> String getValue(epr, handle)

Get a single state value

    Returns a value of a state

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **epr** | **String**| The endpoint reference of the SDC provider | [default to null]
 **handle** | **String**| The descriptor handle of the state | [default to null]

### Return type

[**String**](../Models/string.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain

<a name="setState"></a>
# **setState**
> InvocationResult setState(epr, body)

Set a state on an SDC provider

    Set a specific AbstractState object on the provider

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **epr** | **String**| The endpoint reference of the SDC provider | [default to null]
 **body** | **byte[]**| Xml string representation of the state to set |

### Return type

[**InvocationResult**](../Models/InvocationResult.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/xml
- **Accept**: application/json

<a name="setValue"></a>
# **setValue**
> setValue(epr, handle, value, timeout)

Set a single state value

    Sets a value of a state. This method blocks until the value has been set or an error occurred.

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **epr** | **String**| The endpoint reference of the SDC provider | [default to null]
 **handle** | **String**| The descriptor handle of the state | [default to null]
 **value** | **String**| The value of the state to set | [default to null]
 **timeout** | **Integer**| Timeout in milliseconds to wait until state is modified | [optional] [default to null]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

