# README #

This is a language agnostic REST API description for using the IEEE 11073 SDC communication protocol for point-of-care (PoC) medical devices.
The reason for this specification is to make SDC available in a wide range of programming languages. API clients generated for these languages can make use
of existing SDC frameworks which implement the SDC API server side.

The specification consists of a single Json file __sdcapi.json__ based on OpenAPI. API endpoints are documented below.

Licensed under the Apache License, Version 2.0.

## Documentation for API Endpoints

All URIs are relative to http://\[localhost/\]sdcapi/v

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**getDescription**](Apis/DefaultApi.md#getdescription) | **GET** /provider/{epr}/get/description | Get the static description of an SDC provider.
*DefaultApi* | [**getInvocationStates**](Apis/DefaultApi.md#getinvocationstates) | **GET** /provider/get/invocationstates/{epr}/{id} | Get a list of invocation states.
*DefaultApi* | [**getSdcProviders**](Apis/DefaultApi.md#getsdcproviders) | **GET** /discovery | Get a list of currently active SDC providers.
*DefaultApi* | [**getStates**](Apis/DefaultApi.md#getstates) | **GET** /provider/{epr}/get/states | Get a list of SDC provider states.
*DefaultApi* | [**getValue**](Apis/DefaultApi.md#getvalue) | **GET** /provider/{epr}/get/value/{handle} | Get a single state value
*DefaultApi* | [**setState**](Apis/DefaultApi.md#setstate) | **POST** /provider/{epr}/set/state | Set a state on an SDC provider
*DefaultApi* | [**setValue**](Apis/DefaultApi.md#setvalue) | **POST** /provider/{epr}/set/value/{handle} | Set a single state value


## Documentation for Models

 - [InvocationResult](./\Models/InvocationResult.md)
 - [InvocationState](./\Models/InvocationState.md)

## Documentation for Authorization

All endpoints do not require authorization.

## API client libraries generated from this specification

- [SdcApiSharp](https://bitbucket.org/besting-it/sdcapisharp) - C# Library (nuget link available), supports REST API version 1.2.0

## API server solutions which implement this specification

SdcApi server middleware is needed for any client to work.

### SDCLib/J

- [SDCLib/J](https://bitbucket.org/besting-it/sdclibcontrib) - Repository of Java 11+ software stack (version 9 or later), supports REST API version 1.2.0.
    - [SDCLib/J JAR](http://www.besting-it.de/maven/org/ornet/SDCLib/10.0.2c/SDCLib-10.0.3c-jar-with-dependencies.jar) - Download SDCLib binary JAR version 10.0.3c

How to run SDCLib/J in middleware mode
```
java -cp SDCLib-10.0.3c-jar-with-dependencies.jar org.ornet.sdclib.SDCLib
```

## How to build new API clients for other languages

### Step 1

Use OpenAPI code generator with this specification to generate API client code for a specific target language.

- [OpenAPI generator](https://github.com/OpenAPITools/openapi-generator) - OpenAPI generator (see Download JAR section)

See also the tools listed below.

### Step 2

To make fully use of provided functions, it's strongly recommended to also generate source code from the BICEPS participant and extension point XSD models.

- [BICEPS models](https://standards.ieee.org/standard/11073-10207-2017.html) - 11073-10207 models  (see additional resources for XSD download)

## Get involved!

If you'd like to contribute to this specification, use the issue tracker. You may also create forks of this repository and create pull requests. 
These requests will be reviewed and eventually integrated.

Information about OpenAPI specification format:

- [OpenAPI specification](https://github.com/OAI/OpenAPI-Specification)

### OpenAPI tools

There are a number of tools for editing the specification and generating code. Please refer to

- [OpenAPI tools](https://openapi.tools)
- [OpenAPI online generator](https://openapi-generator.tech)

Official OpenAPI code generator (see JAR Download):

- [Official OpenAPI generator](https://github.com/OpenAPITools/openapi-generator)

## Further information

For information about 11073 SDC please consider the following sources:

- [OR.NET e.V.](http://ornet.org)